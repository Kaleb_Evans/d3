/*
USE master
DROP database D2
CREATE database D2 
*/

USE D2
GO

CREATE TABLE Users (
	UserName varchar(20) NOT NULL,
	FirstName varchar(20) NOT NULL,
	MiddleName varchar(20),
	LastName varchar(20) NOT NULL,
	Password varchar(20) NOT NULL,
	PRIMARY KEY(UserName)
)

CREATE TABLE Client (
	UserName varchar(20) NOT NULL,
	FirstName varchar(20) NOT NULL,
	MiddleName varchar(20),
	LastName varchar(20) NOT NULL,
	Email varchar(20) NOT NULL,
	Phone varchar(20) NOT NULL,
	Password varchar(20) NOT NULL,
	PRIMARY KEY(UserName)
)

CREATE TABLE Type (
	Name varchar(20) NOT NULL,
	Cost varchar(20) NOT NULL,
	Hours varchar(20) NOT NULL,
	PRIMARY KEY(Name)
)

CREATE TABLE Car (
	CarID varchar(20) NOT NULL,
	Make varchar(20) NOT NULL,
	Model varchar(20) NOT NULL,
	Price varchar(20) NOT NULL,
	PRIMARY KEY(CarID)
)

CREATE TABLE Instructor (
	UserName varchar(20) NOT NULL,
	FirstName varchar(20) NOT NULL,
	MiddleName varchar(20),
	LastName varchar(20) NOT NULL,
	Password varchar(20) NOT NULL,
	PRIMARY KEY(UserName)
)

CREATE TABLE TimeSlot (
	ID int NOT NULL,
	TimeSlot smalldatetime,
	PRIMARY KEY(ID,TimeSlot)
)


CREATE TABLE Admin (
	UserName varchar(20) NOT NULL,
	FirstName varchar(20) NOT NULL,
	MiddleName varchar(20),
	LastName varchar(20) NOT NULL,
	Password varchar(20) NOT NULL,
	PRIMARY KEY(UserName)
)

CREATE TABLE Appointment (
	ID varchar(20) NOT NULL PRIMARY KEY,
	Notes varchar(20) NOT NULL
)

CREATE TABLE Document (
	ID varchar(20) NOT NULL PRIMARY KEY,
	Date varchar(20) NOT NULL,
	link varchar(20) NOT NULL,
	Type varchar(20) NOT NULL,
)

CREATE TABLE SocialMedia (
		SocMediaID varchar(3) NOT NULL PRIMARY KEY,
		SocialMediaName varchar(30) NOT NULL
)
	
CREATE TABLE UserSession (
	SessionID int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	SocMediaID varchar(3),
	UserName varchar(20),
	SessionTime int, --assumed as minutes for simplicity
	FOREIGN KEY(SocMediaID) REFERENCES SocialMedia,
	FOREIGN KEY(UserName) REFERENCES Users
)

--Drop statement
DROP TABLE UserSession
DROP TABLE SocialMedia
DROP TABLE Users
DROP TABLE Admin
DROP TABLE TimeSlot
DROP TABLE Car
DROP TABLE Client
DROP TABLE Type
DROP TABLE Instructor
DROP TABLE Appointment
DROP TABLE Document
--//

--This inserts data into Users table
INSERT INTO Users VALUES ('jkc1', 'John', 'Middle', 'Carter', 'pass1')
INSERT INTO Users VALUES ('amo1', 'Amos', 'Carter', 'Orange', 'pass2')
INSERT INTO Users VALUES ('wkc1', 'Wong', 'Caleb', 'Cartel', 'pass3')
INSERT INTO Users VALUES ('ddj1', 'Daisy', 'Day', ' Johnson ', 'pass4')
INSERT INTO Users VALUES ('dps1', 'Dayne', 'Pint', ' Shipper ', 'pass5')
--//

--This inserts data into Car table
INSERT INTO Car VALUES ('01', 'Honda', 'Accord', '$20,000')
INSERT INTO Car VALUES ('02', 'Ford', 'Falcon', '$15,999')
INSERT INTO Car VALUES ('03', 'Holden', 'Commodore', '$6,599')
INSERT INTO Car VALUES ('04', 'Honda', 'Civic', '$8,000')
INSERT INTO Car VALUES ('05', 'Toyota', 'Corolla', '$2,000')
--//

--This inserts data into TimeSlot table
INSERT INTO TimeSlot VALUES ('7832', '2017-05-01 09:00')
INSERT INTO TimeSlot VALUES ('9932', '2017-05-03 15:30')
INSERT INTO TimeSlot VALUES ('2345', '2017-05-05 10:30')
INSERT INTO TimeSlot VALUES ('5463', '2017-05-08 18:00')
INSERT INTO TimeSlot VALUES ('8741', '2017-05-27 08:00')
INSERT INTO TimeSlot VALUES ('6534', '2017-05-28 17:30')
INSERT INTO TimeSlot VALUES ('6666', '2017-05-29 13:30')


--//

--This inserts data into Type table
INSERT INTO Type VALUES ('Mike', '$120', '70')
INSERT INTO Type VALUES ('John', '$3300', '1')
INSERT INTO Type VALUES ('Jessica', '$50', '82')
INSERT INTO Type VALUES ('Clay', '$70,000', '92')
INSERT INTO Type VALUES ('Hannah', '$22', '67')
--//

--This inserts data into Admin table
INSERT INTO Admin VALUES ('mck1', 'Mike', 'Can','Koon','passmck')
INSERT INTO Admin VALUES ('jfd1', 'John', 'Francis','Doe','passjfd')
INSERT INTO Admin VALUES ('tzb1', 'Tony', 'Zeck','Baker','passtzb')
INSERT INTO Admin VALUES ('anz1', 'Anna', 'Nelly','Zambone','passanz')
INSERT INTO Admin VALUES ('bnz1', 'Ben', 'Nevanda','Zamilly','passbnz')
--//

--This inserts data into Instructor table
INSERT INTO Instructor VALUES ('mck1', 'Mike', 'Can','Koon','passmck')
INSERT INTO Instructor VALUES ('jfd1', 'John', 'Francis','Doe','passjfd')
INSERT INTO Instructor VALUES ('tzb1', 'Tony', 'Zeck','Baker','passtzb')
INSERT INTO Instructor VALUES ('anz1', 'Anna', 'Nelly','Zambone','passanz')
INSERT INTO Instructor VALUES ('bnz1', 'Ben', 'Nevanda','Zamilly','passbnz')
--//

--This table links the two tables together
INSERT INTO SocialMedia Values ('FB', 'Facebook')
INSERT INTO SocialMedia Values ('TWT','Twitter')
INSERT INTO SocialMedia Values ('IG', 'Instagram')
INSERT INTO SocialMedia Values ('YT', 'YouTube')
--//

--This stores data into UserSession linking Users and SocialMediaa
INSERT INTO UserSession Values ('FB', 'jkc1', 256)
INSERT INTO UserSession Values ('TWT', 'amo1', 20)
INSERT INTO UserSession Values ('IG', 'wkc1', 60)
INSERT INTO UserSession Values ('YT', 'ddj1', 180)
INSERT INTO UserSession Values ('FB', 'dps1', 25)
--//

--Select statement
Select * FROM Users
Select * FROM SocialMedia
Select * FROM UserSession
Select * FROM Admin
Select * FROM TimeSlot
Select * FROM Car
Select * FROM Client
Select * FROM Type
Select * FROM Instructor
Select * FROM Appointment
Select * FROM Document
--//
